# LeafletJS Heatmap Renderer

Thank you for taking this coding challenge. 

In this task, the following skills are tested: 
1. Ability to use ReactJS
2. Ability to use third-party libraries to render OpenStreetMap data (LeafletJS) 
3. Ability to write and serve REST API endpoints on NodeJS 

Your task is as follows: 
1. Create a page with an element with HeatMap of the routes which are driven the most (basically, a simple leafletJS integration together with heat plugin). 
2. Create a JSON schema describing the necessary data for creating a heatmap 
3. Serve a NodeJS endpoint to get the necessary data and render it on the LeafletJS object 

References to use to speed up: 
1. https://www.patrick-wied.at/static/heatmapjs/example-heatmap-leaflet.html
2. https://github.com/Leaflet/Leaflet.heat

Note: 
Usage of docker will be advantageous
